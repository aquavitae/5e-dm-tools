import React from "react";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import Container from "react-bootstrap/Container";
import Monsters from "./Monsters";
import Shop from "./Shop";

const Index: React.FC = () => {
  return <h2>Home</h2>;
};

const AppRouter: React.FC = () => {
  return (
    <Router>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand>DD 5e DM Tools</Navbar.Brand>
        <Navbar.Collapse>
          <Nav className="mr-auto" variant="pills">
            <LinkContainer to="/monsters">
              <Nav.Link>Monsters</Nav.Link>
            </LinkContainer>
            <LinkContainer to="/shop">
              <Nav.Link>Shop</Nav.Link>
            </LinkContainer>
          </Nav>
          <Nav>
            <LinkContainer to="/login">
              <Nav.Link>Login</Nav.Link>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Container fluid>
        <Route path="/" exact component={Index} />
        <Route path="/monsters/" component={Monsters} />
        <Route path="/shop/" component={Shop} />
      </Container>
    </Router>
  );
};

export default AppRouter;
