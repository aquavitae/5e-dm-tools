import React from "react";
import Container from "react-bootstrap/Container";
import AppRouter from "./components/Router";

const App: React.FC = () => {
  return (
    <Container fluid className="App">
      <AppRouter />
    </Container>
  );
};

export default App;
